# 主程序
from celery import Celery

# 为celery使用django配置文件进行设置
import os

if not os.getenv('DJANGO_SETTINGS_MODULE'):
    os.environ['DJANGO_SETTINGS_MODULE'] = 'meiduo_mall.settings.dev'


# 1、创建celery应用
app = Celery('meiduo')
# 2、导入配置，因为启动文件是在与manage同级目录下启动，所以路径为celery_tasks.config
app.config_from_object('celery_tasks.config')
# 3、声明获取的异步任务对列，每一个包就是一个任务，目录中必须要有一个tasks文件，这就是任务文件
app.autodiscover_tasks([
    'celery_tasks.sms',
    'celery_tasks.email',
    'celery_tasks.html'
])
