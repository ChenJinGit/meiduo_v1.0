# 任务文件
from .yuntongxun.sms import CCP
from . import constants
from celery_tasks.main import app
import logging

logger = logging.getLogger('django')


# 使用app.task()装饰器装饰函数，在视图中调用才有delay
@app.task(name="send_sms_code")
def send_sms_code(mobile, sms_code):
    """
    发送短信验证码的异步任务
    :param mobile: 手机号
    :param sms_code: 短信验证码
    :return:
    """
    ccp = CCP()
    ccp.send_template_sms(mobile, [sms_code, constants.SMS_CODE_REDIS_EXPIRES // 60],
                          constants.SEND_SMS_TEMPLATE_ID)
    # 使用日志记录短信验证码
    logger.debug('%s:%s' % (mobile, sms_code))
