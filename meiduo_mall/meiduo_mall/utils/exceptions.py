import logging

from django.db.utils import DatabaseError
from redis.exceptions import RedisError
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import exception_handler as drf_exception_handler

# 获取在配置文件中定义的日志器logger，用来记录日志
logger = logging.getLogger('django')


def exception_handler(exc, context):
    """项目异常处理
    新增处理数据库异常和Redis异常
    :param exc: 异常
    :param context: 抛出异常的上下文
    :return: Response响应对象
    """
    # 先调用DRF默认的 exception_handler 方法, 对异常进行处理
    # 如果处理成功，会返回一个`Response`类型的对象
    response = drf_exception_handler(exc, context)

    # 如果DRF处理异常失败，则返回None，则执行处理数据库异常和Redis异常处理方法
    if response is None:
        view = context['view']  # 获取出错的视图
        # 判断出错类型是否为数据库异常或Redis异常
        if isinstance(exc, DatabaseError) or isinstance(exc, RedisError):
            # 记录日志
            logger.error('[%s] %s' % (view, exc))
            # 重新构造response响应对象
            response = Response({'message': '服务器内部错误'}, status=status.HTTP_507_INSUFFICIENT_STORAGE)
    # 返回数据给前端
    return response
