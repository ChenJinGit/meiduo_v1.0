# 分页类

from rest_framework.pagination import PageNumberPagination


class StandardResultsSetPagination(PageNumberPagination):
    """自定义一个标准分页类"""
    page_size = 2  # 每页显示2条
    page_size_query_param = 'page_size'  # 查询关键字名称：每页多少条
    page_query_param = 'page'  # 查询关键字名称：第几页
    max_page_size = 20  # 分页数据量的最大值
