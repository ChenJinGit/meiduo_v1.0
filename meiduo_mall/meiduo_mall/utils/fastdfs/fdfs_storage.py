from django.core.files.storage import Storage
from django.conf import settings
from fdfs_client.client import Fdfs_client
from django.utils.deconstruct import deconstructible


@deconstructible
class FastDFSStorage(Storage):
    """自定义文件存储系统，把文件上传到ＦastDFS中"""

    def __init__(self, base_url=None, client_conf=None):
        """
        初始化
        :param base_url: 用于构造图片完整路径使用，图片服务器的域名
        :param client_conf: FastDFS客户端配置文件的路径
        """
        # 所有设置都应该从django.conf.settings中获取
        if base_url is None:
            base_url = settings.FDFS_URL
        self.base_url = base_url
        if client_conf is None:
            client_conf = settings.FDFS_CLIENT_CONF
        self.client_conf = client_conf

    def _open(self, name, mode='rb'):
        """
        用不到打开文件，所以省略
        """
        pass

    def _save(self, name, content):
        """
        在FastDFS中保存文件
        :param name: 传入的文件名
        :param content: 文件内容
        :return: 保存到数据库中的FastDFS的文件名
        """
        # 创建一个Fdfs操作对象
        client = Fdfs_client(self.client_conf)
        # 上传文件，参数需要为字节类型，所有要read()
        ret = client.upload_by_buffer(content.read())
        # 判断是否上传成功
        if ret.get('Status') != "Upload successed.":
            raise Exception("upload file failed")
        # 获取FastDFS保存的文件的路径
        file_name = ret.get("Remote file_id")
        # 返回文件名
        return file_name

    def url(self, name):
        """
        返回文件的完整URL路径
        :param name: 数据库中保存的文件名
        :return: 完整的URL
        """
        return self.base_url + name

    def exists(self, name):
        """
        判断文件是否存在，FastDFS可以自行解决文件的重名问题
        所以此处返回False，告诉Django上传的都是新文件，不需要Djang去判断
        :param name:  文件名
        :return: False
        """
        return False
