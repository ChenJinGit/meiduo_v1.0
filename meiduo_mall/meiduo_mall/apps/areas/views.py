from django.shortcuts import render
from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.viewsets import ReadOnlyModelViewSet
from rest_framework_extensions.cache.mixins import CacheResponseMixin
from areas.models import Area
from areas.serializers import AreasSerializer, SubAreaSerializer


# class AreasView(ListAPIView):
#     """查询所有行政划区"""
#     # 指定查询集
#     queryset = Area.objects.filter(parent=None)
#     # 指定序列化器
#     serializer_class = AreasSerializer
#
#
# class AreaView(RetrieveAPIView):
#     """查询市、区"""
#     # 指定查询集
#     queryset = Area.objects.all()
#     # 指定序列化器
#     serializer_class = SubAreaSerializer
class AreasViewSet(CacheResponseMixin, ReadOnlyModelViewSet):
    """行政区划信息"""
    # 区划信息不分页
    pagination_class = None

    def get_queryset(self):
        """重写获取查询集"""
        if self.action == 'list':
            return Area.objects.filter(parent=None)
        else:
            return Area.objects.all()

    def get_serializer_class(self):
        """重写获取序列化器类方法"""
        if self.action == 'list':
            return AreasSerializer
        else:
            return SubAreaSerializer
