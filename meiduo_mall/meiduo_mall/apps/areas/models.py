from django.db import models


class Area(models.Model):
    """行政区划模型类"""
    name = models.CharField(max_length=20, verbose_name='名称')
    # null表示表字段允许为空，blank表示后台增改允许为空
    # on_delete=models.SET_NULL设置为空，即不删除
    # related_name='subs' 自关联字段名
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')

    class Meta:
        db_table = 'tb_areas'
        verbose_name = '行政区划'
        verbose_name_plural = '行政区划'

    def __str__(self):
        return self.name
