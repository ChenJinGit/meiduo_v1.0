from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from areas import views

urlpatterns = [
    # url(r'^areas/$', views.AreasView.as_view()),
    # url(r'^areas/(?P<pk>\d+)/$', views.AreaView.as_view()),
]

# 创建路由对象
router = DefaultRouter()
# 注册路由
router.register(r'areas', views.AreasViewSet, base_name='areas')
# 添加路由
urlpatterns += router.urls
