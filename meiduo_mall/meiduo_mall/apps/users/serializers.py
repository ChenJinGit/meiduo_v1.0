import re
from rest_framework import serializers
from django_redis import get_redis_connection

from celery_tasks.email.tasks import send_verify_email
from goods.models import SKU
from users import constants
from users.models import User, Address


class CreateUserSerializer(serializers.ModelSerializer):
    """创建用户序列化器"""
    # 声明校验字段
    password2 = serializers.CharField(label='密码', write_only=True)
    sms_code = serializers.CharField(label='短信验证码', write_only=True)
    allow = serializers.CharField(label='同意协议', write_only=True)
    token = serializers.CharField(label="jwt token", read_only=True)

    class Meta:
        # 指定关联的模型类
        model = User
        # 指定对应的属性
        fields = ('id', 'username', 'password', 'password2', 'sms_code', 'mobile', 'allow', 'token')
        # 修改字段选项参数
        extra_kwargs = {
            'username': {
                'min_length': 5,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许5-20个字符的用户名',
                    'max_length': '仅允许5-20个字符的用户名',
                }
            },
            'password': {
                'write_only': True,
                'min_length': 8,
                'max_length': 20,
                'error_messages': {
                    'min_length': '仅允许8-20个字符的密码',
                    'max_length': '仅允许8-20个字符的密码',
                }
            }
        }

    def validate_allow(self, value):
        """校验用户是否同意协议"""
        if value != 'true':
            raise serializers.ValidationError('请同意用户协议')
        return value

    def validate_mobile(self, value):
        """校验手机号"""
        # 校验手机号格式
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式不正确')
        # 校验手机号唯一性
        count = User.objects.filter(mobile=value).count()
        if count > 0:
            raise serializers.ValidationError('手机号已注册')
        return value

    def validate_username(self, value):
        """校验用户名"""
        count = User.objects.filter(username=value).count()
        if count > 0:
            raise serializers.ValidationError('用户名已存在')
        return value

    def validate(self, attrs):
        """校验密码与短信验证码"""
        password = attrs.get('password')
        password2 = attrs.get('password2')
        if password != password2:
            raise serializers.ValidationError('两次输入密码不一致')

        # 判断短信验证码是否一致
        # 获取短信验证码
        sms_code = attrs.get('sms_code')
        mobile = attrs.get('mobile')
        # 获取redis链接对象
        redis = get_redis_connection('verify')
        # 获取redis中对应的短信验证码
        real_sms_code = redis.get('sms_code_%s' % mobile)
        # 判断短信验证码是否过期
        if not real_sms_code:
            raise serializers.ValidationError('短信验证码已过期')
        # 解码
        real_sms_code = real_sms_code.decode()
        # 判断短信验证码是否一致
        if sms_code != real_sms_code:
            raise serializers.ValidationError('短信验证码错误')
        return attrs

    def create(self, validated_data):
        """新增数据"""
        # 移除数据库中模型类不存在的字段
        del validated_data['password2']
        del validated_data['sms_code']
        del validated_data['allow']

        # 使用模型类保存数据
        user = super().create(validated_data)
        # 设置密码加密
        user.set_password(validated_data['password'])
        user.save()

        # 注册成功了，使用jwt保存登陆状态
        from rest_framework_jwt.settings import api_settings

        # 获取 生成载荷的函数
        jwt_payload_handler = api_settings.JWT_PAYLOAD_HANDLER
        # 获取 生成token的函数
        jwt_encode_handler = api_settings.JWT_ENCODE_HANDLER

        payload = jwt_payload_handler(user)
        token = jwt_encode_handler(payload)

        user.token = token
        return user


class UserDetailSerializer(serializers.ModelSerializer):
    """用户中心用户详细信息序列化器"""

    class Meta:
        model = User
        fields = ['id', 'username', 'mobile', 'email', 'email_active']


class EmailSerializer(serializers.ModelSerializer):
    """邮箱序列化器"""

    class Meta:
        model = User
        fields = ['id', 'email']
        extra_kwargs = {
            'email': {
                'required': True
            }
        }

    def update(self, instance, validated_data):
        """修改email字段的值"""
        instance.email = validated_data['email']
        instance.save()

        # 生成邮箱验证链接地址
        verify_url = instance.generate_verify_email_url()
        # 发送邮件
        send_verify_email.delay(instance.email, verify_url)
        return instance


class UserAddressSerializer(serializers.ModelSerializer):
    """用户收货地址序列化器"""
    # 用于显示给客户端浏览器的地址信息[增加__str__方法的关联]
    province = serializers.StringRelatedField(read_only=True)
    city = serializers.StringRelatedField(read_only=True)
    district = serializers.StringRelatedField(read_only=True)

    # 设置保存到数据表的id信息[取消关联]
    province_id = serializers.IntegerField(label='省ID', required=True)
    city_id = serializers.IntegerField(label='市ID', required=True)
    district_id = serializers.IntegerField(label='区ID', required=True)

    class Meta:
        model = Address
        # 除外这些字段，其他字段为序列化器使用
        exclude = ['user', 'is_deleted', 'create_time', 'update_time']

    # 校验手机号
    def validate_mobile(self, value):
        if not re.match(r'^1[3-9]\d{9}$', value):
            raise serializers.ValidationError('手机号格式错误')
        return value

    def create(self, validated_data):
        validated_data['user'] = self.context['request'].user
        return super().create(validated_data)


class PasswordSerializer(serializers.ModelSerializer):
    """修改密码序列化器"""
    current_password = serializers.CharField(label='当前密码', write_only=True)
    password2 = serializers.CharField(label='确认密码', write_only=True)

    class Meta:
        model = User
        fields = ['current_password', 'password', 'password2']

    def validate(self, attrs):
        # print(attrs)
        # print(self.instance)
        # 校验当前密码
        # print(self.context['request'].user)
        if not self.instance.check_password(attrs['current_password']):
            raise serializers.ValidationError('当前密码错误')
        print('密码正确')
        # 校验两次密码是否一致
        password = attrs['password']
        password2 = attrs['password2']
        if password != password2:
            raise serializers.ValidationError('两次密码输入不一致')
        return attrs

    def update(self, instance, validated_data):
        # 先保存数据
        instance.password = validated_data['password']
        # 再设置密码
        instance.set_password(validated_data['password'])
        instance.save()
        return instance


class AddUserBrowsingHistorySerializer(serializers.Serializer):
    """
    添加用户浏览历史序列化器
    """
    sku_id = serializers.IntegerField(label='商品SKU编号', min_value=1)

    def validate_sku_id(self, value):
        """检查sku_id"""
        try:
            SKU.objects.get(id=value)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('该商品不存在')
        return value

    def create(self, validated_data):
        # 获取当前登陆的用户id
        user_id = self.context['request'].user.id
        sku_id = validated_data['sku_id']
        """redis中保存历史记录的数据类型格式

        例如，本次用户访问的sku是2,

        user_id: [
                    7,6,1,4,2
                 ]
        """
        # 把sku_id保存redis的list类型数据中
        redis_conn = get_redis_connection('history')
        pl = redis_conn.pipeline()
        # 删除redis中同样的sku_id,
        pl.lrem('history_%s' % user_id, 0, sku_id)
        # 然后添加sku_id到redis
        pl.lpush('history_%s' % user_id, sku_id)
        # 如果超过最大长度，则最后一个成员要被剔除
        pl.ltrim('history_%s' % user_id, 0, constants.USER_BROWSING_HISTORY_COUNTS_LIMIT - 1)
        pl.execute()
        return validated_data
