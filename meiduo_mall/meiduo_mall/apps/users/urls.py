from django.conf.urls import url
from rest_framework.routers import DefaultRouter

from users import views
from rest_framework_jwt.views import obtain_jwt_token

urlpatterns = [
    # 校验用户名唯一性
    url(r'^usernames/(?P<username>\w{5,20})/count/$', views.UsernameCountView.as_view()),
    # 校验手机号唯一性
    url(r'^mobiles/(?P<mobile>1[3-9]\d{9})/count/$', views.MobileCountView.as_view()),
    # 用户注册
    url(r'^users/$', views.UserView.as_view()),
    # 用户登陆签发JWT
    # jwt提供的登陆认证视图[调整以后的登陆视图，新增了合并购物车功能]
    # url(r'^authorizations/$', obtain_jwt_token),
    url(r'^authorizations/$', views.UserAuthorizeView.as_view()),
    # 用户详细信息
    url(r'^user/$', views.UserDetailView.as_view()),
    # 保存邮箱
    url(r'^email/$', views.EmailView.as_view()),
    # 验证邮箱
    url(r'^emails/verification/$', views.VerifyEmailView.as_view()),
    # 设置默认地址
    url(r'^addresses/<pk>/status/$', views.AddressesViewSet.as_view({'put': 'status'})),
    # 保存收货地址标题
    # url(r'^addresses/<pk>/title/$', views.AddressesViewSet.as_view({'put': 'title'})),
    url(r'^addresses/(?P<pk>\d+)/title/$', views.AddressesViewSet.as_view({'put': 'title'})),
    # 密码修改
    url(r'^password/(?P<pk>\d+)/$', views.PasswordView.as_view()),
    # 获取用户浏览商品记录
    url(r'^browse_histories/$', views.UserBrowsingHistoryView.as_view()),
]

router = DefaultRouter()
router.register(r'addresses', views.AddressesViewSet, base_name='addresses')
urlpatterns += router.urls
