from django.shortcuts import render

# Create your views here.
from django_redis import get_redis_connection
from rest_framework import status
from rest_framework.decorators import action
from rest_framework.generics import CreateAPIView, RetrieveAPIView, UpdateAPIView
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet
from rest_framework_jwt.views import ObtainJSONWebToken

from carts.utils import merge_cart_cookie_to_redis
from goods.models import SKU
from goods.serializers import SKUSerializer
from users.models import User
from users.serializers import CreateUserSerializer, UserDetailSerializer, EmailSerializer, UserAddressSerializer, \
    PasswordSerializer, AddUserBrowsingHistorySerializer
from users.utils import check_verify_email_token
from . import constants


class UsernameCountView(APIView):
    """用户名数
    请求方法：get
    url:usernames/(?P<username>\w{5, 20})/count/
    获取参数：username
    返回参数：username， count
    """

    def get(self, request, username):
        # 查询数据库
        count = User.objects.filter(username=username).count()
        # 构造返回参数
        data = {
            'username': username,
            'count': count
        }
        # 返回数据
        return Response(data)


class MobileCountView(APIView):
    """手机号数
    请求方法：get
    url:mobiles/(?P<mobile>1[3-9]\d{9})/count
    获取参数：mobile
    返回参数：mobile， count
    """

    def get(self, request, mobile):
        # 查询数据库
        count = User.objects.filter(mobile=mobile).count()
        # 构造返回参数
        data = {
            'mobile': mobile,
            'count': count
        }
        return Response(data)


class UserView(CreateAPIView):
    """
    用户注册
    请求方法：post
    url: /users/
    获取参数：username、password、password2、sms_code、mobile、allow
    返回数据：id、username、mobile
    """
    serializer_class = CreateUserSerializer


class UserDetailView(RetrieveAPIView):
    """查询一条用户数据"""
    # 指定使用的序列化器
    serializer_class = UserDetailSerializer
    # 访问视图必须要求用户已通过认证（即登录之后）
    permission_classes = [IsAuthenticated]

    # 指定查询集
    def get_object(self):
        return self.request.user


class EmailView(UpdateAPIView):
    """用户中心保存邮箱"""
    # 指定使用的序列化器
    serializer_class = EmailSerializer
    # 访问视图必须要求用户已通过认证（即登录之后）
    permission_classes = [IsAuthenticated]

    # 指定查询集
    def get_object(self):
        return self.request.user


class VerifyEmailView(APIView):
    """邮箱验证"""

    def get(self, request):
        # 1.获取参数token
        token = request.query_params.get('token')
        # 判断是否获取到
        if not token:
            return Response({'message': '无效的token'}, status=status.HTTP_400_BAD_REQUEST)
        # 2.校验token，获取到user_id
        user_id = check_verify_email_token(token)
        # 判断是否获取到
        if not user_id:
            return Response({'message': '无效的token'}, status=status.HTTP_400_BAD_REQUEST)
        # 3.根据user_id获取用户信息
        try:
            user = User.objects.get(pk=user_id)
        except Exception:
            return Response({'message': '无用户信息'}, status=status.HTTP_400_BAD_REQUEST)
        # 4.修改email_active状态
        user.email_active = True
        user.save()
        # 5.返回数据
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)


class AddressesViewSet(ModelViewSet):
    """用户收货地址视图集"""
    # 设置权限验证
    permission_classes = [IsAuthenticated]

    # 指定查询集， 查询所有非逻辑删除的
    # queryset = Address.objects.filter(is_deleted=False)
    # 指定序列化器
    serializer_class = UserAddressSerializer

    def get_queryset(self):
        return self.request.user.addresses.filter(is_deleted=False)

    def list(self, request, *args, **kwargs):
        """重写list方法"""
        queryset = self.get_queryset()
        # print('查询集：%s' % queryset)
        serializer = self.get_serializer(queryset, many=True)
        # print('序列化器对象：%s' % serializer)
        # 获取当前用户
        user = self.request.user
        # print('当前用户：%s' % user)
        # print('addresses字典： %s' % serializer.data)
        return Response({
            'user_id': user.id,
            'addresses': serializer.data,
            'limit': constants.USER_ADDRESS_COUNTS_LIMIT,
            'default_address_id': user.default_address_id
        })
        # return Response({'message': 'ok'}, status=status.HTTP_200_OK)

    def create(self, request, *args, **kwargs):
        # 校验增加收货地址条目数不超过最大限制
        # 查询当前用户收货地址数
        count = request.user.addresses.count()
        if count >= constants.USER_ADDRESS_COUNTS_LIMIT:
            return Response({'message': '保存收货地址数超过限制'}, status=status.HTTP_400_BAD_REQUEST)
        return super().create(request, *args, **kwargs)

    def destroy(self, request, *args, **kwargs):
        # 获取当前请求中pk值对应的收货地址
        address = self.get_object()
        # 逻辑删除
        address.is_deleted = True
        address.save()
        return Response({'message': 'OK'}, status=status.HTTP_204_NO_CONTENT)

    # 通过自定义action方法，实现默认地址设置
    # url:addresses/<pk>/status/
    @action(methods=['put'], detail=True)
    def status(self, request, pk):
        """设置默认地址"""
        address = self.get_object()
        # 将当前地址保存到user表中
        request.user.default_address = address
        request.user.save()
        return Response({'message': 'OK'}, status=status.HTTP_200_OK)

    # url:addresses/<pk>/title/
    @action(methods=['put'], detail=True)
    def title(self, request, pk):
        """保存标题"""
        # 获取当前操作的pk值对应的数据
        address = self.get_object()
        address.title = request.data.get('title')
        address.save()
        return Response({'message': 'ok'}, status=status.HTTP_200_OK)


class PasswordView(UpdateAPIView):
    """修改密码"""
    serializer_class = PasswordSerializer
    queryset = User.objects.all()

    # def get_object(self):
    #     return self.request.user


class UserBrowsingHistoryView(CreateAPIView):
    """用户浏览器商品的视图"""
    serializer_class = AddUserBrowsingHistorySerializer
    permission_classes = [IsAuthenticated]

    def get(self, request):
        """获取浏览历史记录"""
        # 从redis中获取当前用户的user_id
        user_id = request.user.id

        # 从redis获取浏览历史记录
        redis_conn = get_redis_connection('history')
        history = redis_conn.lrange('history_%s' % user_id, 0, constants.USER_BROWSING_HISTORY_COUNTS_LIMIT - 1)

        skus = []
        # 为了保持查询出的顺序与用户的浏览历史保存顺序一致
        for sku_id in history:
            sku = SKU.objects.get(id=sku_id)
            skus.append(sku)
        serializer = SKUSerializer(skus, many=True)
        return Response(serializer.data)


class UserAuthorizeView(ObtainJSONWebToken):
    """用户认证[在原有的jwt登陆方法中补充合并购物车功能]"""
    def post(self, request, *args, **kwargs):
        # 调用父类的方法，获取drf jwt扩展默认的认证用户处理结果
        response = super().post(request, *args, **kwargs)
        # 仿照drf jwt扩展对于用户登录的认证方式，判断用户是否认证登录成功
        # 如果用户登录认证成功，则合并购物车
        serializer = self.get_serializer(data=request.data)
        if serializer.is_valid():
            user = serializer.object.get('user') or request.user
            # 补充合并购物车的代码
            response = merge_cart_cookie_to_redis(request, user, response)
        return response
