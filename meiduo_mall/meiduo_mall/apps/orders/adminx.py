import xadmin
from goods.adminx import SKUAdmin
from . import models


class OrderAdmin(object):

    # 　设置自动刷新字段的时间
    list_display = ['order_id', 'create_time', 'total_amount', 'pay_method', 'status']
    refresh_times = [3, 5]  # 可选以支持按多长时间(秒)刷新页面

    data_charts = {
        "order_amount": {'title': '订单金额', "x-field": "create_time", "y-field": ('total_amount',),
                         "order": ('create_time',)},
        "order_count": {'title': '订单量', "x-field": "create_time", "y-field": ('total_count',),
                        "order": ('create_time',)},
    }

    # 生成统计表
    readonly_fields = ['sales', 'comments']

xadmin.site.register(models.OrderInfo, OrderAdmin)