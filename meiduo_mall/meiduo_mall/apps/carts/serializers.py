from rest_framework import serializers

from goods.models import SKU


class CartSerializer(serializers.Serializer):
    """购物车数据序列化器"""

    sku_id = serializers.IntegerField(label='sku_id', min_value=1)
    count = serializers.IntegerField(label='加入购物车的商品数量', min_value=1)
    selected = serializers.BooleanField(label='商品勾选状态', default=True)

    def validate(self, attrs):
        try:
            sku = SKU.objects.get(id=attrs['sku_id'])
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')
        return attrs


class CartSKUSerializer(serializers.ModelSerializer):
    """购物车商品数据序列化器"""
    count = serializers.IntegerField(label='商品数量')
    selected = serializers.BooleanField(label='是否勾选')

    class Meta:
        model = SKU
        fields = ['id', 'name', 'count', 'price', 'default_image_url', 'selected']


class CartDelsteSerializer(serializers.Serializer):
    """删除购物车数据序列话器"""
    sku_id = serializers.IntegerField(label='商品id', min_value=1)

    def validate_sku_id(self, attrs):
        try:
            sku = SKU.objects.get(id=attrs)
        except SKU.DoesNotExist:
            raise serializers.ValidationError('商品不存在')
        return attrs


class CartSelectAllSerializer(serializers.Serializer):
    """
    购物车全选
    """
    selected = serializers.BooleanField(label='全选')
