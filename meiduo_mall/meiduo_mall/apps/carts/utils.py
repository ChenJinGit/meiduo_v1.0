import base64
import pickle
from django_redis import get_redis_connection


def merge_cart_cookie_to_redis(request, user, response):
    """
        登陆后合并购物车[从cookie中覆盖到redis中]
        :param request: 本次登陆成功以后的请求对象[用于获取cookie]
        :param user: 本次登陆的用户，用于获取redis购物车
        :param response: 本次登陆成功以后的响应对象[用于设置cookie]
        :return: response　响应对象[因为我们在函数内部操作了cookie需要返回给客户端]
        """
    # 获取cookie中的购物车
    cookie_cart = request.COOKIES.get('cart')
    # 如果cookie没有购物车商品信息，则接下来不需要合并了，直接返回
    if cookie_cart is None:
        return response

    # 解析cookie购物车数据
    cookie_cart = pickle.loads(base64.b64decode(cookie_cart.encode()))
    """
    {
      sku_id1: {"count": 2, "selected":True,}
      sku_id2: {"count": 1, "selected":True,}
    }
    """
    redis_conn = get_redis_connection('cart')
    pl = redis_conn.pipeline()
    # 直接遍历cookie中的购物车，把同样sku_id的数据设置到redis中
    for sku_id, sku_dict in cookie_cart.items():
        # 1. 先覆盖cart_<user_id>对应的sku商品信息
        # hset　　设置hash数据中指定键中对应的下标的值
        pl.hset('cart_%s' % user.id, sku_id, int(sku_dict['count']))
        if sku_dict['selected']:
            pl.sadd('cart_selected_%s' % user.id, sku_id)
        else:
            pl.srem('cart_selected_%s' % user.id, sku_id)
    pl.execute()

    # 遍历覆盖完成以后，直接删除cookie
    response.delete_cookie('cart')
    return response
