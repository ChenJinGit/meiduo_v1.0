from rest_framework import serializers
from django_redis import get_redis_connection

from oauth.models import OAuthQQUser
from oauth.utils import check_save_user_token
from users.models import User


class QQAuthUserSerializer(serializers.Serializer):
    """QQ登录注册序列化器"""
    # 声明字段
    mobile = serializers.RegexField(regex=r'^1[3-9]\d{9}$', label='手机号')
    password = serializers.CharField(min_length=8, max_length=20, label='密码')
    sms_code = serializers.CharField(label='短信验证码')
    access_token = serializers.CharField(label='操作凭证')

    def validate(self, attrs):
        """校验参数"""
        # 校验access_token
        # 获取参数
        access_token = attrs['access_token']
        # 调用check_save_user_token校验access_token，获取openid
        openid = check_save_user_token(access_token)
        # 判断是否获取到openid
        if not openid:
            raise serializers.ValidationError({'message': '无效的access_token'})
        # 保存openid
        attrs['openid'] = openid

        # 检验短信验证码
        sms_code = attrs['sms_code']
        mobile = attrs['mobile']
        # 获取redis链接对象
        redis = get_redis_connection('verify')
        # 从redis数据库中获取真实的短信验证码
        try:
            real_sms_code = redis.get('sms_code_%s' % mobile)
            real_sms_code = real_sms_code.decode()
        except Exception:
            # 判断短信验证码是否过期
            raise serializers.ValidationError({'message': '短信验证码已过期'})
        # 比较传入的短信验证码和真实短信验证码是否一致
        if sms_code != real_sms_code:
            raise serializers.ValidationError({'message': '短信验证码错误'})

        # 如果用户存在，校验密码
        try:
            user = User.objects.get(mobile=mobile)
        except User.DoesNotExist:
            pass
        else:
            # 校验密码
            password = attrs['password']
            if not user.check_password(password):
                raise serializers.ValidationError({'message': '密码错误'})
            # 保存当前用户
            attrs['user'] = user
        return attrs

    def create(self, validated_data):
        # 如果可能会获取不到，使用get()，使用[]获取不到会报错
        user = validated_data.get('user')
        if not user:
            # create_user实现了密码加密
            user = User.objects.create_user(
                username=validated_data['mobile'],
                password=validated_data['password'],
                mobile=validated_data['mobile']
            )
        # 将用户绑定openid
        OAuthQQUser.objects.create(
            openid=validated_data['openid'],
            user=user
        )
        # 向视图对象中补充user对象属性，以便在视图中使用user
        self.context['view'].user = user
        # 返回用户数据
        return user
