from django.contrib import admin
from . import models
# Register your models here.
# 注册模型类，注册后才会在管理后台显示
admin.site.register(models.ContentCategory)
admin.site.register(models.Content)