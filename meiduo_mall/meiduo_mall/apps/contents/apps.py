from django.apps import AppConfig


class ContentsConfig(AppConfig):
    name = 'contents'
    # 后台站点标题显示
    verbose_name = '广告'
