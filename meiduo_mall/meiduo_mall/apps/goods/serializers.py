from rest_framework import serializers
from drf_haystack.serializers import HaystackSerializer
from .models import SKU
from .search_indexes import SKUIndex


class CategorySerializer(serializers.Serializer):
    id = serializers.IntegerField(label="ID")
    name = serializers.CharField(label="分类")


class ChannelSerializer(serializers.Serializer):
    url = serializers.CharField(read_only=True, label="链接地址")
    category = CategorySerializer()


class SKUSerializer(serializers.ModelSerializer):
    class Meta:
        model = SKU
        fields = ["id", "name", "price", "comments", "default_image_url"]


class SKUIndexSerializer(HaystackSerializer):
    """
    SKU索引结果数据序列化器[数据必须先经过haystack处理]
    """
    object = SKUSerializer(read_only=True)

    class Meta:
        # 前面声明的索引类
        index_classes = [SKUIndex]
        # 索引类中使用的字段
        fields = ('text', 'object')
