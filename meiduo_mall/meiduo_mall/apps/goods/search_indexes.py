from haystack import indexes

from .models import SKU

# 索引类的类名格式是：数据模型类名+Index
class SKUIndex(indexes.SearchIndex, indexes.Indexable):
    # 设置允许请求搜索时的参数名称和参数格式
    # document=True, use_template=True　表示当前text并非模型/数据表中的text字段，而是多个字段的集合
    # 因此我们还需要设置一个字段模板，用于配置text对应的是哪些字段的集合
    # 字段模板必须按照要求创建指定目录下存储　
    # 　　　　　　模板目录/search/indexes/当前子应用目录名/模型类名小写_搜索参数.txt
    text = indexes.CharField(document=True, use_template=True)
    id = indexes.IntegerField(model_attr='id')
    name = indexes.CharField(model_attr='name')
    price = indexes.CharField(model_attr='price')
    default_image_url = indexes.CharField(model_attr='default_image_url')
    comments = indexes.IntegerField(model_attr='comments')

    # 指定搜索的模型类，通过get_model设置
    def get_model(self): # 这里一句是固定，除了返回值---->模型类名
        """返回建立索引的模型类"""
        return SKU

    # 设置当前模型中，哪些数据可以被用户搜索，通过index_queryset方法来设置
    def index_queryset(self, using=None):
        """返回要建立索引的数据查询集"""
        # is_launched=True 要求商品一定上架以后才能被搜索出来
        return self.get_model().objects.filter(is_launched=True)
