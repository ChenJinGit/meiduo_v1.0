import xadmin
from xadmin import views
from celery_tasks.html.tasks import generate_static_sku_detail_html
from celery_tasks.html.tasks import generate_static_list_search_html
from . import models



class BaseSetting(object):
    """xadmin的基本配置"""
    enable_themes = True  # 开启主题切换功能
    use_bootswatch = True


xadmin.site.register(views.BaseAdminView, BaseSetting)


class GlobalSettings(object):
    """xadmin的全局配置"""
    site_title = "美多商城运营管理系统"  # 设置站点标题
    site_footer = "美多商城集团有限公司"  # 设置站点的页脚
    menu_style = "accordion"  # 设置菜单折叠


xadmin.site.register(views.CommAdminView, GlobalSettings)


class SKUAdmin(object):
    # 图标
    model_icon = 'fa fa-gift'
    # 列表字段显示
    list_display = ['id', 'name', 'price', 'stock', 'sales', 'comments']
    # 搜索的字段
    search_fields = ['id', 'name']
    # 字段的过滤
    list_filter = ['category']
    # 点击修改的字段
    list_editable = ['price', 'stock']
    # 点击显示详情的字段
    show_detail_fields = ['name']
    #
    show_bookmarks = True

    # 到处表的格式
    list_export = ['xls', 'csv', 'xml']

    def save_models(self):
        self.new_obj.save()
        generate_static_sku_detail_html.delay(self.new_obj.id)


xadmin.site.register(models.SKU, SKUAdmin)


class GoodsCategoryAdmin(object):
    """商品分类站点管理类"""

    def save_models(self):
        """钩子方法，在运营站点中编辑/添加数据时，会自动执行代码"""
        # 保存操作
        self.new_obj.save()
        # 保存操作后，执行异步任务
        generate_static_list_search_html.delay()

    def delete_model(self):
        """钩子方法，在删除数据的时候自动执行"""
        self.obj.delete()
        generate_static_list_search_html.delay()


class GoodsChannelAdmin(object):
    """商品频道站点管理类"""

    def save_models(self):
        """钩子方法，在运营站点中编辑/添加数据时，会自动执行代码"""
        # 保存操作
        self.new_obj.save()
        # 保存操作后，执行异步任务
        generate_static_list_search_html.delay()

    def delete_model(self):
        """钩子方法，在删除数据的时候自动执行"""
        self.obj.delete()
        generate_static_list_search_html.delay()

class SKUSpecificationAdmin(object):
    def save_models(self):
        generate_static_sku_detail_html.delay()

    def delete_model(self):
        sku_id = self.obj.sku.id
        self.obj.delete()
        generate_static_sku_detail_html.delay(sku_id)


class SKUImageAdmin(object):
    def save_models(self):
        self.new_obj.save()
        generate_static_sku_detail_html.delay(self.new_obj.sku.id)
        # 设置ＳＫＵ默认图片
        sku = self.new_obj.sku
        if not sku.default_image_url:
            sku.default_image_url = self.new_obj.image.url
            sku.save()

    def delete_model(self):
        sku_id = self.obj.sku.id
        self.obj.delete()
        generate_static_sku_detail_html.delay(sku_id)


# 注册模型类
xadmin.site.register(models.GoodsCategory, GoodsCategoryAdmin)
xadmin.site.register(models.GoodsChannel, GoodsChannelAdmin)
xadmin.site.register(models.Goods)
xadmin.site.register(models.Brand)
xadmin.site.register(models.GoodsSpecification)
xadmin.site.register(models.SpecificationOption)
xadmin.site.register(models.SKUSpecification, SKUSpecificationAdmin)
xadmin.site.register(models.SKUImage, SKUImageAdmin)

